import pytest
from hot16types import *
from treemaker import TreeMaker 
from datetime import datetime
import urllib

def test_parse():
    sample_input = """
            <div class="cardContainer" id="Solar">
            <div class="miniatureImageContainer">
                <img  src="https://www.cgm.pl/wp-content/uploads/2018/01/Solar1500.jpg"  class="miniatureImage"/>
            </div>

            <a href="/artysta/Solar" style="text-decoration: none;"><h2 class="cardName" style="color: teal;">Solar</h2></a>

            <h4>28.04.2020</h4>

                                        
            <a href="/artysta/Solar"><button class="topButton">Przejdź</button></a>

            <h3 class="cardNominees">Nominowani:</h3>

                                                <a href='/#Białas'><button class="cardButton  confirmed ">Białas</button></a>
                                                                <a href='/#Taco Hemingway'><button class="cardButton  confirmed ">Taco Hemingway</button></a>
                                                                <a href='/#Bedoes'><button class="cardButton  confirmed ">Bedoes</button></a>
                                                                <a href='/#Mata'><button class="cardButton  confirmed ">Mata</button></a>
                                                                <a href='/#Mike Posner'><button class="cardButton ">Mike Posner</button></a>
	    </div>
	    """

    trmkr = TreeMaker()
    submission = trmkr.parse(sample_input)[0]
    assert submission.artist ==  "Solar"
    assert submission.nominated_by == None
    assert submission.yt_link == "https://www.youtube.com/watch?v=fG3Mzg3bkgs"
    assert submission.nominees ==  ["Białas", "Taco Hemingway", "Bedoes",
                                    "Mata", "Mike Posner"]
    assert submission.date == datetime(2020, 4, 28) 



    
	

