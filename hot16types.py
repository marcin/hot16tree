from collections import namedtuple
from anytree import NodeMixin
""" Data types used in hot16tree """

# A named tuple for passing submission data
Hot16 = namedtuple('Hot16', ['artist',
                             'nominated_by', 
                             'nominees', 
                             'date',
                             'yt_link',
                             'view_count'])

class Hot16Node(NodeMixin):
    """ A Hot16 submission as an anytree node """
    def __init__(self, artist, date, yt_link, view_count, parent=None, children=None):
        self.artist = artist
        self.date = date
        self.yt_link = yt_link
        self.view_count = view_count
        self.parent = parent
        if children:
          self.children = children
