""" Parser for extracting artist data for hot16challenge2 
TODO:
    3. Make timeline"""

from hot16types import *
from datetime import datetime
from bs4 import BeautifulSoup
import re
import anytree
import urllib

class TreeMaker():
    """ parses data from website """
    def __init__(self, link="https://hot16challenge2.pl"):
        self.link = link

    def parse(self, text):
        """ Parses website HTML to get submission data """
        out = []
        soup = BeautifulSoup(text, features='lxml')

        # get all (don't ask why it's called cardContainer)
        submissions = soup.find_all("div", {"class": "cardContainer"})

        # get data
        for submission in submissions:
            name = submission['id'] 
            nominated_by = submission.find('a', {"class": "cardNominatedByLink"})
            # the first artist will not have a nominated_by
            nominated_by = nominated_by.string if nominated_by is not None else None
            # to get all nominees we need to look at the buttons
            nominees = []
            nominee_buttons = submission.find_all('button', {"class": "cardButton"})
            for nominee_button in nominee_buttons:
                nominees.append(nominee_button.string)
            datestr = re.search('[0-9]+\.[0-9]+\.[0-9]+', submission.prettify()).group(0)
            date = datetime.strptime(datestr, '%d.%m.%Y')
            # youtube data is scraped in two subfunctions
            youtube_link = self.get_yt_link(name)
            view_count = self.get_yt_viewcount(youtube_link)
            # view_count = None
            out.append(Hot16(name, nominated_by, nominees, date, youtube_link, view_count))

        # sort list to make sure we won't have items without parents later on
        return out[::-1]

    def get_yt_link(self, artist_name):
        # this is a dirty hack
        artist_link = urllib.parse.quote(self.link + "/artysta/" + artist_name,
                                   safe=':/')
        artist_page = urllib.request.urlopen(artist_link).read().decode('utf-8')
        # find youtube video number
        youtube_no = re.search('\/embed\/(\S+)\"',
                               artist_page)
        if youtube_no:
            youtube_link = 'https://youtube.com/watch?v=' + \
                            youtube_no.group(1)
        else:
            youtube_link = None

        return youtube_link
        
    def get_yt_viewcount(self, youtube_link):
        if youtube_link:
            youtube_html = urllib.request.urlopen(youtube_link).read().decode('utf-8')
            # go to youtube and try to get view count
            view_count =  re.search(r'\"viewCount\\":\\"(\d*)\\",',
                                    youtube_html)
            if view_count:
                view_count = view_count.group(1)
            else:
                print("Couldn't find view_count")
                view_count = None

            print(view_count)
        else:
            view_count = None
            print("Youtube link is None")

        return int(view_count) if view_count else None

    def fix_data(self, submissions):
        """ Fix data with issues """
        for submission in submissions:
            if submission.artist.lower() == "dj remisz":
                submissions.append(Hot16("Remisz", submission.nominated_by,
                                   submission.nominees, submission.date,
                                   submission.yt_link, submission.view_count))
                submissions.remove(submission)
            if submission.artist.lower() == "$ierra":
                submissions.append(Hot16("Sierra", submission.nominated_by,
                                   submission.nominees, submission.date,
                                   submission.yt_link, submission.view_count))
                submissions.remove(submission)
            if submission.nominated_by == "$ierra":
                submissions.append(Hot16(submission.artist, "Sierra",
                                   submission.nominees, submission.date,
                                   submission.yt_link, submission.view_count))
                submissions.remove(submission)
            if submission.nominated_by == "Hermes":
                submissions.remove(submission)
            if submission.nominated_by == "Augustyn, Rzepa ŁDZ":
                submissions.append(Hot16(submission.artist, "Augustyn",
                                   submission.nominees, submission.date,
                                   submission.yt_link, submission.view_count))
                submissions.remove(submission)

        return submissions

    def build_tree(self, submissions):
        """ Uses list of submissions (of type Hot16) to create a tree of all
        submissions """
        # define a dummy node as root - we may have multiple roots, this helps
        # keep it all in one tree
        root = Hot16Node(artist="Zbiórka!", date =
                         datetime(2020, 4, 28), yt_link =
                         'https://www.siepomaga.pl/hot16challenge', view_count
                         = 10e6)

        # then build the tree. I do not assume any structure in the data. For
        # each item try to find the parent nodes by name. We're going through
        # the list until everything is arranged
        while submissions:
            for submission in submissions:
                def filterfun(node):
                    if submission.nominated_by is None:
                        return False
                    else:
                        return node.artist.lower() == submission.nominated_by.lower()

                if submission.nominated_by is None:
                    parent = root
                else:
                    parent = anytree.search.find(root, filter_= filterfun)

                if parent:
                    submissions.remove(submission)
                    node = Hot16Node(artist=submission.artist,
                                     date=submission.date,
                                     view_count = submission.view_count,
                                     yt_link = submission.yt_link,
                                     parent=parent)
        return root


