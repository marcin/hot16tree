from treemaker import TreeMaker
import urllib
from anytree.exporter import DotExporter
import pickle
import numpy as np
import anytree

MIN_FONT = 10
MAX_FONT = 100

trmkr = TreeMaker()

# scrape data from website
submissions = trmkr.parse(urllib.request.urlopen("https://hot16challenge2.pl").read())
# fix issues with data (inconsistent names etc.)
submissions = trmkr.fix_data(submissions) 
# create tree from data
tree = trmkr.build_tree(submissions)
# save it for later use
with open('tree.pkl', 'wb+') as handle:
    pickle.dump(tree, handle, pickle.HIGHEST_PROTOCOL)
# export tree as DOT with relevant parameters
# calculate relevant stats
view_count_list = [int(node.view_count) if node.view_count else 0 for node in anytree.LevelOrderIter(tree)]
view_count_array = np.array(view_count_list)
view_count_max = np.max(view_count_array)

def nodeattrfunc(node):
    """ makes nodes pretty """
    if node.view_count is None:
        node.view_count = 0
    # determine font size
    font_size = int(MIN_FONT + float(node.view_count) / view_count_max * (MAX_FONT -
                                                                     MIN_FONT)) 
    font_size_attr = "fontsize=" + str(font_size) 
    if node.yt_link:
        attrs = font_size_attr + ', URL="' + node.yt_link + '"'
    else:
        attrs = font_size_attr
    return attrs

def nodenamefunc(node):
    return node.artist

DotExporter(tree, 
            nodeattrfunc = nodeattrfunc,
            nodenamefunc = nodenamefunc,
           ).to_dotfile("hot16challenge2.dot")
